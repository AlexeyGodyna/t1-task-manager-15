package ru.t1.godyna.tm.exception.field;

import ru.t1.godyna.tm.exception.AbstractException;

public class AbsractFieldException extends AbstractException {

    public AbsractFieldException() {
    }

    public AbsractFieldException(final String message) {
        super(message);
    }

    public AbsractFieldException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbsractFieldException(final Throwable cause) {
        super(cause);
    }

    public AbsractFieldException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
