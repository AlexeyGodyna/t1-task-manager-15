package ru.t1.godyna.tm.api.controller;

public interface IProjectController {

    void clearProjects();

    void createProject();

    void removeProjectById();

    void removeProjectByIndex();

    void showProjectById();

    void showProjectByIndex();

    void showProjects();

    void updateProjectById();

    void updateProjectByIndex();

    void startProjectById();

    void startProjectByIndex();

    void changeProjectStatusByIndex();

    void changeProjectStatusById();

    void completeProjectById();

    void completeProjectByIndex();

}
